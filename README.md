# 概要

Cordova + React + OnsenUIで作ったサンプルプロジェクトです。

## 必要な環境

予め以下をインストールして下さい。

### ビルドに必要なもの
- Cordova (6.0以上で確認)
- Android SDK (Androidアプリをビルドする場合）
- Xcode (iosアプリをビルドする場合)

### ソースコードのコンパイルに必要なもの
ソースコードを修正してコンパイルする場合は以下のツールが必要になります。

- browserify (nodeのnpmでインストールして下さい）

## ビルド方法

~~~
git clone https://bitbucket.org/kojiro526/cordova-hello
cd cordova-hello

# Androidのビルド+実行
cordova run android

# iosアプリのビルド
cordova build ios
~~~
iosアプリをビルドするためには、上記コマンドでビルドした後、Xcodeで以下のディレクトリ配下のプロジェクトを読み込んでビルドします。

- ./platform/ios

## ソースコードの修正
ソースコードを修正してみたい場合は、以下の手順で行います。

1. `./src`配下のソースコードを修正
2. ソースコードをコンパイル
    - 以下のコマンドで編集したソースコードをアプリで読み込むファイルにコンパイル
    `browserify ./src/main.js -o ./www/js/index.js -t [ babelify --presets [ es2015 react ] ]`