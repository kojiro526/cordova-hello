'use strict';

let React = require('react');
let ReactDOM = require('react-dom');
let ons = require('onsenui');
let Ons = require('react-onsenui');

/**
 * リストページのサンプル
 */
let Page2 = React.createClass({
    PropTypes: {
        /**
         * ツールバーのレンダリングは、スプリッターメニューを開くアイコンがあるため、
         * 呼び出し元から与えられたコールバック関数に委ねている。
         */
        renderToolbar: React.PropTypes.func.isRequired
    },
    render: function(){
        return (
            <Ons.Page
                renderToolbar={this.props.renderToolbar}
                >
                <div className="center">
                Page2
                </div>
            </Ons.Page>
        );
    }
});
module.exports = Page2;