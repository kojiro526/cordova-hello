'use strict';

let React = require('react');
let ReactDOM = require('react-dom');
let ons = require('onsenui');
let Ons = require('react-onsenui');
let TopPageTab = require('./TopPageTab.jsx');

/**
 * アプリのトップページ
 *
 * スプリッターメニューを構成する。
 */
let TopPage = React.createClass({
    getInitialState: function() {
        return {
            is_open: false
        };
    },
    _handleShowMenu: function(){
        this.setState({is_open: true});
    },
    _handleHideMenu: function(){
        this.setState({is_open: false});
    },
    render: function(){
        return (
            <Ons.Page>
            <Ons.Splitter>
                <Ons.SplitterSide
                    id='menu'
                    collapse={true}
                    side='left'
                    width={280}
                    isSwipeable={true}
                    isOpen={this.state.is_open}
                    onClose={this._handleHideMenu}
                    onOpen={this._handleShowMenu}
                >
                    <Ons.Page>
                    MENU
                    </Ons.Page>
                </Ons.SplitterSide>
                <Ons.SplitterContent
                    id='content'
                    >
                    <TopPageTab
                        ref="topPageTab"
                        key='top_page_tab'
                        onShowMenu={this._handleShowMenu}
                        navigator={this.props.navigator} />
                </Ons.SplitterContent>
            </Ons.Splitter>
            </Ons.Page>
        );
    }
});
module.exports = TopPage;
