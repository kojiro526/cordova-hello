'use strict';

let React = require('react');
let ReactDOM = require('react-dom');
let ons = require('onsenui');
let Ons = require('react-onsenui');
let Page1 = require('./Page1.jsx');
let Page2 = require('./Page2.jsx');

/**
 * トップページのタブを構成。
 */
let TopPageTab = React.createClass({
    propTypes: {
        onShowMenu: React.PropTypes.func.isRequired,
        navigator: React.PropTypes.object.isRequired
    },
    getInitialState: function(){
        return {
            index: 0
        }
    },
    /**
     * スプリッターメニューを表示するコールバック
     */
    _handleShowMenu: function(){
        this.props.onShowMenu();
    },

    /**
     * ツールバーを描画
     */
    _renderToolbar: function() {
        return (
            <Ons.Toolbar>
                <div className='left'>
                    <Ons.ToolbarButton onClick={this._handleShowMenu}>
                        <Ons.Icon icon='ion-navicon' />
                    </Ons.ToolbarButton>
                </div>
                <div className='center'>
                    Hello Cordova
                </div>
            </Ons.Toolbar>
        );
    },

    /**
     * タブを描画
     */
    _renderTabs: function(){
        return [
            {
                content: <Page1 key='page1' renderToolbar={this._renderToolbar} navigator={this.props.navigator} />,
                tab: <Ons.Tab key='tab1' label='Page1' />
            },{
                content: <Page2 key='page2' renderToolbar={this._renderToolbar} navigator={this.props.navigator} />,
                tab: <Ons.Tab key='tab2' label='Page2' />
            }
        ];
    },
    render: function(){
        return (
            <Ons.Tabbar
                index={this.state.index}
                key='top_page_tabbar'
                onPreChange={(event) =>
                    {
                        if (event.index != this.state.index){
                            this.setState({index: event.index});
                        }
                    }
                }
                renderTabs={this._renderTabs}
                hide-tabs={false}
            />
        );
    }
});
module.exports = TopPageTab;
