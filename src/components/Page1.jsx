'use strict';

let React = require('react');
let ReactDOM = require('react-dom');
let ons = require('onsenui');
let Ons = require('react-onsenui');

/**
 * ページのサンプル
 */
let Page1 = React.createClass({
    PropTypes: {
        /**
         * ツールバーのレンダリングは、スプリッターメニューを開くアイコンがあるため、
         * 呼び出し元から与えられたコールバック関数に委ねている。
         */
        renderToolbar: React.PropTypes.func.isRequired
    },
    getInitialState: function() {
        return {
            /**
             * フォームに入力された値は以下のstateに保存される。
             */
            title: '',
            content: ''
        };
    },    
    _handleClickSubmit: function(){
        // ボタンをクリックした際の処理
        ons.notification.alert({
            message: 'title: ' + this.state.title + "\n" + "content: " + this.state.content
        });
    },
    render: function(){
        return (
            <Ons.Page
                renderToolbar={this.props.renderToolbar}
                >
                <div className="main-form">
                    <section style={{textAlign: 'center'}}>
                        <p>
                            <Ons.Input
                                value={this.state.title}
                                modifier="underbar"
                                float
                                placeholder="Title"
                                onChange={(event)=>{ this.setState({title: event.target.value}) }}
                                />
                        </p>
                        <p>
                            <textarea
                                rows={8}
                                value={this.state.content}
                                onChange={(event)=>{ this.setState({content: event.target.value}) }}
                                />
                        </p>
                    </section>
                    <div style={{textAlign: 'center'}}>
                        <Ons.Button
                            modifier="default"
                            onClick={this._handleClickSubmit}
                            >
                            送信
                        </Ons.Button>
                    </div>
                </div>
            </Ons.Page>
        );
    }
});
module.exports = Page1;
