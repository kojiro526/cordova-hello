'use strict';

let React = require('react');
let ReactDOM = require('react-dom');
let ons = require('onsenui');
let Ons = require('react-onsenui');
let TopPage = require('./TopPage.jsx');

/**
 * 最初に呼ばれるコンポーネント
 * 
 * OnsenUIのNavigatorを使い、新しく開いたページはすべてNavigatorの
 * スタックに追加されていくようにする。
 */
let MainPage = React.createClass({
    /**
     * navigatorにpushPageした際に、pushするページをレンダリングする。
     */
    renderPage: function(route, navigator) {

        const props = route.props || {};

        /**
         * 2016-09-25 sasaki@tobila
         * navigatorで
         * Aページ -> Bページ -> Cページ -> Bページ
         * のように遷移（スタック）した時に、Bページ同士でkeyが同じ場合、重複してしまう。
         * そのため、topページ以外は連番でkeyを振ることとした。
         * 今後、navigator.pushPage()する時にkeyの指定は不用（指定してもここで上書きされる）
         */
        if(props.key != 'top_page'){
            props.key = 'nav-child' + navigator.routes.length;
            props.pageKey = props.key;
        }

        props.navigator = navigator;
        return React.createElement(route.component, props);        
    },

    render: function(){
        return (
            <Ons.Navigator
                id='mainNavigator'
                key='navigator'
                ref='navigator'
                renderPage={this.renderPage}
                initialRoute={{
                    component: TopPage,
                    props: {
                        key: 'top_page'
                    },
                    hasBackButton: false,
                }}
                />
        );
    }
});
module.exports = MainPage;
