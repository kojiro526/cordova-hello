'use strict';

let React = require('react');
let ReactDOM = require('react-dom');
let ons = require('onsenui');
let Ons = require('react-onsenui');
let MainPage = require('./components/MainPage.jsx');

/**
 * 以下はCordovaプロジェクトを生成した際に自動的に作られる index.js の内容をコピーしている。
 * 
 * cordovaが完全に読み込まれた際に deviceready が火しするため、cordovaのコードは
 * onDeviceReady 内に書く。
 */
let app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // cordova プラグインの処理などはここに書く
    }
}
app.initialize();

/**
 * 以下は OnsenUIが読み込まれた際の処理。
 * その中でReactを実行している。
 */
ons.ready(function(){
    window.reactComponent = ReactDOM.render(
        <MainPage />,
        document.getElementById('app')
    );
});
